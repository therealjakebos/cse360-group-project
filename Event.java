import java.util.ArrayList;

//Structure for an Event

public class Event {
	private String activityName;
	private int duration;
	
	private ArrayList<Event> dependencies;
	private ArrayList<Event> ancestors;
	private ArrayList<String> strDependencies;
	private boolean head;
	
	public Event(){	
	}
	
	
	public Event(String activityName, int duration){
		this.activityName = activityName;
		this.duration = duration;
		this.head = true;
	}
	
	public Event(String activityName, int duration, ArrayList<String> strDependencies){
		this.activityName = activityName;
		this.duration = duration;
		this.strDependencies = strDependencies;
		this.head = false;
	}
	
	public String getName(){
		return this.activityName;
	}
	
	public void setName(String name){
		this.activityName = name;
	}
	
	public int getDuration(){
		return this.duration;
	}
	
	public void setDuration(int duration)
	{
		this.duration = duration;
	}
	
	public ArrayList<Event> getDependencies(){
		if(this.dependencies == null){
			this.dependencies = new ArrayList<Event>();
		}
		return this.dependencies;
	}
	
	public void setDependencies(ArrayList<Event> dependencies){
		this.dependencies = dependencies;
	}
	
	public ArrayList<Event> getAncestors(){
		if(this.ancestors == null){
			this.ancestors = new ArrayList<Event>();
		}
		return this.ancestors;
	}
	
	public void setAncestors(ArrayList<Event> ancestors){
		this.ancestors = ancestors;
	}

	public boolean headValue() {
		return head;
	}

	public void setHead(boolean head) {
		this.head = head;
	}

	public ArrayList<String> getStrDependencies() {
		return strDependencies;
	}

	public void setStrDependencies(ArrayList<String> strDependencies) {
		this.strDependencies = strDependencies;
	}
	
	public void addAncestor(Event n){
		if(this.ancestors == null){
			this.ancestors = new ArrayList<Event>();
		}
		this.ancestors.add(n);
	}
}
