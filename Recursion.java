import java.util.ArrayList;
import java.io.IOException;

class recursive{

	void test(){
		ArrayList<Event> aDep = new ArrayList<Event>();
		ArrayList<Event> bDep = new ArrayList<Event>();
		ArrayList<Event> cDep = new ArrayList<Event>();
		ArrayList<Event> dDep = new ArrayList<Event>();
		ArrayList<Event> eDep = new ArrayList<Event>();
		ArrayList<Event> fDep = new ArrayList<Event>();
		ArrayList<Event> allNodes = new ArrayList<Event>();
		
		
		Event A = new Event("a",1);
		
		Event B = new Event();
		B.setHead(false);
		B.setName("b");
		B.setDuration(2456);
		Event C = new Event();
		C.setHead(false);
		C.setName("c");
		B.setDuration(3);
		Event D = new Event();
		D.setHead(false);
		D.setName("d");
		B.setDuration(4);
		Event E = new Event();
		E.setHead(false);
		E.setName("e");
		B.setDuration(5);
		Event F = new Event();
		F.setHead(false);
		F.setName("f");
		B.setDuration(6);
		
		bDep.add(A);
		//bDep.add(C);
		//dDep.add(B);
		cDep.add(B);
		dDep.add(C);
		eDep.add(D);
		fDep.add(E);

		
		//A.setDependencies(aDep);
		B.setDependencies(bDep);
		C.setDependencies(cDep);
		D.setDependencies(dDep);
		E.setDependencies(eDep);
		F.setDependencies(fDep);
		
		
		
		allNodes.add(A);
		allNodes.add(B);
		allNodes.add(C);
		allNodes.add(D);
		allNodes.add(E);
		allNodes.add(F);
		
		EventList list = EventList.getInstance();
		list.setEventList(allNodes);
		Organizer sort = new Organizer(list);
		
		sort.recursiveStartPath();
		sort.preparePathList();
		
		//sort.checkCycle();
		
		//System.out.println(sort.isValid());
		//System.out.println(sort.getCode());
		
		Report report = new Report(sort);
		try {
			report.createReport("test");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Could not create output file");
		}
		
		
		
		/*
		ArrayList<PathData> pathList = thing.getPathList();
		assertTrue(pathList.size()==2);
		*/
	}
}

