import java.util.ArrayList;
import java.util.Collections;

// Organizer Class

//the class that holds all of the logic 
public class Organizer
{
    private ArrayList<Path> pathList; 
    private EventList list;
    private boolean valid;
    private static int index;
    private static int errorCode;
    
    public Organizer(EventList list)
    {
        this.setList(list);
        errorCode = -1;
        valid = true;
    }
    
    /*
     * Public void Functions
     */
    
    /**
     * Checks all error scenarios and quits if one is found
     */
    public void checkAll()
    {
    	int functionIndex = 0;
    	while(errorCode == -1)
    	{
    		checkHelper(functionIndex);
    		functionIndex++;
    	}
    }
    
    /**
     * Prepares Numbers in pathList
     */
	public void preparePathList() 
	{
		sumPaths();
		insertionSort();
	}
	
	/**
	 * Public function to find paths
	 */
	public void recursiveStartPath()
	{
		ArrayList<Event> heads = list.getHeads();
		ArrayList<Path> paths = new ArrayList<Path>();
		for(Event head : heads)
		{
			ArrayList<Path> individualPaths = new ArrayList<Path>();
			index = -1;
			getPaths(head, individualPaths);
			paths.addAll(individualPaths);
		}
		this.pathList = paths;
	}
	
	/**
	 * ReCalculates value of paths
	 * @param duration of Node to choose
	 * @return if successful
	 */
	public boolean reCalculate(String name, int duration)
	{
		EventList x = EventList.getInstance();
		boolean flag = false;
		for(Event y: x.getEventList())
		{
			if(name.equals(y.getName()))
			{
				y.setDuration(duration);
				flag = true;
				break;
			}
		}
		if(flag == false)
		{
			return false;
		}
		for(Path z: pathList)
		{
			z.setDuration();
		}
		return true;
	}
	/*
	 * Public non void functions
	 */
	
	/**
	 * Returns names of activities in paths
	 */
	public ArrayList<ArrayList<String>> getNames()
	{
		ArrayList<ArrayList<String>> fullNames = new ArrayList<ArrayList<String>>();
		for(int i = 0; i < this.pathList.size(); i++)
		{
			ArrayList<String> arr = new ArrayList<String>();
			for(int j = 0; j < this.pathList.get(i).path.size(); j++)
			{
				arr.add(this.pathList.get(i).path.get(j).getName());
			}
			fullNames.add(arr);
		}
		return fullNames;
	}
	
	/**
	 * Returns duration of paths
	 */
	public ArrayList<Integer> getDurations()
	{
		ArrayList<Integer> durations = new ArrayList<Integer>();
		for(int i = 0; i < this.pathList.size(); i++)
		{
			durations.add(pathList.get(i).duration);
		}
		return durations;
	}

    
  
	/*
	 * Error Checks
	 */
	
	
    /*
     * This function should check to make sure that all Dependencies of a Nodes dependencies exists
     * and then set the dependencies for the node
     * 
     * for each node : get the strDependenices, then you can verify that a node exists for each dependency
     * (maybe with a boolean array), and then once you are sure that they all exists, double check my 
     * createDependencies function I just created and call that
     */
	//This function test to make sure that all dependency names exist in the nodeList exist
    public void checkDependencies() {
    	ArrayList<Event> nodeList = this.list.getEventList();
    	//go through all nodes in the node list
    	for(int i = 0; i < nodeList.size();i++) {
    		Event n = nodeList.get(i);
    		if(!n.headValue())
    		{
    			//cycle through dependencies of n
	    		for(int j = 0; j < n.getStrDependencies().size();j++) 
	    		{
	    			boolean nameInNodeList = false;
	    			String depName = n.getStrDependencies().get(j);
	    			//cycle through all nodes in nodeList searching for depName
	    			for(int k = 0; k < nodeList.size();k++) {
	    				if(nodeList.get(k).getName().equals(depName)) {
	    					nameInNodeList = true;
	    				}
	    			}
	    			if(!nameInNodeList) {
	    				//depName not in nodeList
	    				errorCode = 1;
	    				this.valid = false; 
	    				break;
	    			}
	    		}
	    		
	    		if(valid)
	    		{
	    			ArrayList<String> dep = n.getStrDependencies();
	    			createDependencies(dep, n);
	    		}
    		}
    		if(!this.valid)
    		{
    			break;
    		}
    		
    	}
    }    
    
    public void checkEmpty()
    {
    	if(this.list.getEventList().isEmpty())
    	{
    		this.valid = false;
    		errorCode = 4;
    	}
    }

    public void checkAllNodesConnected() 
    {
    	ArrayList<Event> nodeList = this.list.getEventList();
    	//cycles through all nodes in nodeList
    	for(int i = 0; i < nodeList.size(); i++) 
    	{
    		Event n = nodeList.get(i);
    		//if node is not a startNode and nodeList does not contain
    		// the node is unconnected
    		if(!n.headValue())
    		{
    			if(n.getDependencies() == null)
    			{
        			this.valid = false;
        			errorCode = 3;
    			}
    		}

    	}
    }
    
    /**
     * Checks to make sure no circular paths exist
     */
    public void checkForCycle() {
    	ArrayList<Event> nodeList = this.list.getEventList();
    	//create a tail node
    	//create copy of nodeList
    	ArrayList<Event> tempList = new ArrayList<Event>();
    	for(int i = 0; i < nodeList.size(); i++) { 
    		tempList.add(nodeList.get(i));
    	}
    	//find node that are not dependencies of other node
    	for(int i = 0; i < nodeList.size(); i++ ) {
    		Event n = nodeList.get(i);
    		for(int j = 0; j < n.getDependencies().size(); j++) {
    			Event d = n.getDependencies().get(j);
    			if(tempList.contains(d)) {
    				tempList.remove(d);
    			}
    		}
    	}
    	if(tempList.isEmpty()) {
    		errorCode = 2;
			valid = false;
    	}
    	else {
    		//set tail to tempList
        	Event tail = new Event();
        	tail.setDependencies(tempList);
        	//call tracePath on tail node
        	tracePath(tail, new ArrayList<Event>());
    	}
    	
    	
    }
    //will set the error code and valid fields
    private boolean tracePath(Event node, ArrayList<Event> tracedPath){
    	if(tracedPath.contains(node)) {
    		errorCode = 2;
			valid = false;
			return false;
    	}
    	tracedPath.add(node);
    	if(node.headValue()) {
    		return true;
    	}
    	for(int i = 0; i < node.getDependencies().size(); i++) {
    		Event d = node.getDependencies().get(i);
    		ArrayList<Event> clonedArrayList = (ArrayList<Event>) tracedPath.clone();
    		tracePath(d, clonedArrayList);
    	}
    	return true;
    }


    /**
     * Checks for duplicates
     */
    public void errorDuplicateCheck()
    {
    	/*
    	 * If the function is already valid, we don't need to test it
    	 */
        if(valid == true)
        {
        	boolean exists = false;
	        /*ArrayList<String> names = new ArrayList<String>();
	        for(int i=0; i < list.list.size(); i++)
	        {
	        	names.add(list.list.get(i).getName());
	        }
	        */
	        for(int i = 0; i < list.list.size(); i ++)
	        {
	            for(int j = i+1; j < list.list.size(); j++ )
	            {
	                if(list.list.get(i).getName().equals(list.list.get(j).getName()))
	                {
	                    valid = false;
	                    errorCode = 0;
	                    break;
	                }
	            }
	            /*
	            if(!exists)
	            {
	                names.add(list.list.get(i).getName());
	            }
	            else
	            {
	            	break;
	            }*/
	        }
        }
    }
    



	
	/*
	 * Private functions
	 */
	
    /**
     * Magic
     */
	private void getPaths(Event head, ArrayList<Path> paths)
	{
		if(index == -1)
		{
			ArrayList<Event> headList = new ArrayList<Event>();
			headList.add(head);
			Path headPath = new Path(headList);
			paths.add(headPath);
			index++;
		}
		ArrayList<Event> ancestors = head.getAncestors();
		if(ancestors.size()>0)
		{
			Path data = paths.remove(index);
			for(int i = 0; i < ancestors.size();i++)
			{	
				ArrayList<Event> empty = new ArrayList<Event>();
				Path dependPath = new Path(empty);
				for(Event x: data.path)
				{
					dependPath.path.add(x);
				}
				dependPath.path.add(ancestors.get(i));
				paths.add(dependPath);
				getPaths(ancestors.get(i), paths);
			}
		}
		else
		{
			index++;
		}
	}
	
	/**
	 * Iterates through errorCodes
	 * @param functionIndex
	 */
	private void checkHelper(int functionIndex)
    {
    	switch(functionIndex)
    	{
	    	case(0):
	    		errorDuplicateCheck();
	    		break;
	    	case(1):
	    		checkEmpty();
	    		break;
	    	case(2):
	    		checkDependencies();
	    		break;
	    	case(3):
	    		checkForCycle();
	    		break;
	    	case(4):
	    		checkAllNodesConnected();
	    		break;
	    	default:
	    		errorCode = -2;
	    		
    	
    	}
    }
	
	/**
	 * Sets dependencies for Node given string list and Node
	 */
	private void createDependencies(ArrayList<String> arr, Event n) //dependencies precede the activity
	{
		ArrayList<Event> depend = new ArrayList<Event>();
		for(String node: arr)
		{
			for(Event x: this.list.list)
			{
				if(node.equals(x.getName()))
				{
					depend.add(x);
					x.addAncestor(n);
				}
			}
		}
		n.setDependencies(depend);
	}
	
	/**
	 * Sets ancestors for Node given string list and Node
	 */
	private void createAncestors(ArrayList<String> arr, Event n) //ancestors come after activity (could be called children)
	{
		ArrayList<Event> depend = new ArrayList<Event>();
		for(String node: arr)
		{
			for(Event x: this.list.list)
			{
				if(node.equals(x.getName()))
				{
					depend.add(x);
				}
			}
		}
		n.setAncestors(depend);
	}
	
	/**
	 * Sums each path in pathList
	 */
	private void sumPaths()
	{
		for(Path x: this.pathList)
		{
			x.setDuration();
		}
	}
	
	/**
	 * Insertion sort to organize durations
	 */
	private void insertionSort() 
	{  
	   for (int i = 0; i < this.pathList.size(); i++) 
	   { 
		   for(int j = i+1; j < this.pathList.size(); j++)
		   {
			   if(pathList.get(i).duration < pathList.get(j).duration)
			   {
				   Collections.swap(pathList, i, j);
			   }
		   }
	   } 
	}
	
    /*
     * Getters and Setters
     */
	public boolean isValid() 
	{
		return valid;
	}

	public void setValid(boolean valid) 
	{
		this.valid = valid;
	}

	public ArrayList<Path> getPathList() 
	{
		return pathList;
	}

	public void setPathList(ArrayList<Path> pathList) 
	{
		this.pathList = pathList;
	}

	public EventList getList() 
	{
		return list;
	}

	public void setList(EventList list) 
	{
		this.list = list;
	}
	
	/**
	 * gets critical paths in PathData area
	 * @return PathData critical value
	 * @return null if more than one critical path
	 */
	public ArrayList<Path> getCritical()
	{
		int max = -1;
		sumPaths();
		ArrayList<Path> critical = new ArrayList<Path>();
		for(Path path: pathList)
		{
			if(path.duration > max)
			{
				max = path.duration;
				critical.clear();
				critical.add(path);
			}
			else if(path.duration == max)
			{
				critical.add(path);
			}
		}
		return critical;
	}
	
	public ArrayList<ArrayList<String>> getCriticalNames(ArrayList<Path> info)
	{
		ArrayList<ArrayList<String>> fullNames = new ArrayList<ArrayList<String>>();
		for(int i = 0; i < info.size(); i++)
		{
			ArrayList<String> arr = new ArrayList<String>();
			for(int j = 0; j < info.get(i).path.size(); j++)
			{
				arr.add(info.get(i).path.get(j).getName());
			}
			fullNames.add(arr);
		}
		return fullNames;
	}
	
	public ArrayList<Integer> getCriticalDuration(ArrayList<Path> info)
	{
		ArrayList<Integer> durations = new ArrayList<Integer>();
		for(int i = 0; i < info.size(); i++)
		{
			durations.add(info.get(i).duration);
		}
		return durations;
	}
	
	public static int getErrorCode()
	{
		return errorCode;
	}
}