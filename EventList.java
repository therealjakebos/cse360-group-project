import java.util.ArrayList;

public class EventList 
{
		private static EventList instance = null;
		
	   	public ArrayList<Event> list = new ArrayList<Event>();
	   	
		private EventList() 
	   	{ 
	         
	   	} 
	  
	    // static method to create instance of EventList class 
	    public static EventList getInstance(){ 
	        if (instance == null) 
	        {
	            instance = new EventList(); 
	        }
 	        return instance; 
	    }
	    
	    //set list value
	    public void setEventList(ArrayList<Event> list){
	    	this.list = list;
	    }
	    
	    //return list
	    public ArrayList<Event> getEventList(){
	    	return this.list;
	    }
	    
	    //reset list
	    public void resetList(){
	    	this.instance = null;
	    }
	    
	    //add node to list
	    public void addToList(Event task){
	    	list.add(task);
	    }
	    
	    public ArrayList<Event> getHeads(){
	    	ArrayList<Event> heads = new ArrayList<Event>();
	    	for(Event x: this.list){
	    		if(x.headValue() == true){
	    			heads.add(x);
	    		}
	    	}
	    	return heads;
	    }
}
