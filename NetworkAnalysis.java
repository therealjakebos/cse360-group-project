import java.awt.Frame;
import javax.swing.*;

public class NetworkAnalysis extends JApplet {
	public void init(){
		MainUI mNodeEntryUIPanel=new MainUI();
		getContentPane().add(mNodeEntryUIPanel);
		setSize(800, 400);
		setVisible(true);
		Frame appletFrame = (Frame)this.getParent().getParent();
		appletFrame.setTitle("Network Analyzer");
	}

	public static void endApplication(){
		System.exit(0);
	}
}
