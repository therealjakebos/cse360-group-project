//File for creating the JPanel - Main Screen of the program
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.*;
import java.awt.*;

public class MainUI extends JPanel{
	public JLabel eventLabel, durationLabel, dependenciesLabel;
	public JButton addEvent, process, help, about, restart, quit;
	public JTextField eventName, eventDuration, eventDependencies;
	public JRadioButton startingEvent, criticalPaths;
	public JPanel entryPanel, entryPanel2, buttonPanel;
	public static boolean copOut = false;
	public static JFrame analysisFrame = new JFrame();
	


	public void main(String args[]) {
	        EventQueue.invokeLater(new Runnable() {
	            @Override
	            public void run() {
	                try {
	                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
	                    ex.printStackTrace();
	                }

	                JFrame frame = new JFrame("Testing");
	                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	                frame.add(new MainUI());
	                frame.pack();
	                frame.setLocationRelativeTo(null);
	                frame.setVisible(true);
	            }
	        });
	}

	
	
	public MainUI() {
		//setting up components for MainUI
		setLayout(new GridLayout(1,2));
		entryPanel= new JPanel();
		entryPanel2=new JPanel();
		buttonPanel=new JPanel();
		addEvent=new JButton();
		addEvent.setIcon(new ImageIcon("C:\\Users\\Jake\\eclipse-workspace\\CSE360 Network Analyzer\\src\\resources\\plus.png"));
		process=new JButton();
		process.setIcon(new ImageIcon("C:\\Users\\Jake\\eclipse-workspace\\CSE360 Network Analyzer\\src\\resources\\play.png"));
		about=new JButton();
		about.setIcon(new ImageIcon("C:\\Users\\Jake\\eclipse-workspace\\CSE360 Network Analyzer\\src\\resources\\info.png"));
		help=new JButton();
		help.setIcon(new ImageIcon("C:\\Users\\Jake\\eclipse-workspace\\CSE360 Network Analyzer\\src\\resources\\help.png"));
		restart=new JButton();
		restart.setIcon(new ImageIcon("C:\\Users\\Jake\\eclipse-workspace\\CSE360 Network Analyzer\\src\\resources\\restart.png"));
		quit=new JButton();
		quit.setIcon(new ImageIcon("C:\\Users\\Jake\\eclipse-workspace\\CSE360 Network Analyzer\\src\\resources\\quit.png"));
		startingEvent=new JRadioButton();
		criticalPaths=new JRadioButton();
		eventName=new JTextField();
		eventDuration=new JTextField();
		eventDependencies=new JTextField();
		eventLabel=new JLabel();
		durationLabel=new JLabel();
		dependenciesLabel=new JLabel();
		
		//continuation to set up MainUI components
		entryPanel.setLayout(new GridLayout(3,1));
		entryPanel.add(eventLabel);
		entryPanel.add(eventName);
		entryPanel.add(durationLabel);
		entryPanel.add(eventDuration);
		entryPanel.add(dependenciesLabel);
		entryPanel.add(eventDependencies);
		add(entryPanel);
		add(startingEvent);
		add(criticalPaths);
		buttonPanel.setLayout(new GridLayout(6,1));
		buttonPanel.add(addEvent);
		buttonPanel.add(process);
		buttonPanel.add(about);
		buttonPanel.add(help);
		buttonPanel.add(restart);
		buttonPanel.add(quit);
		add(buttonPanel);
		
		
		//adding components to the UI
		addEvent.setText("Add Event");
		process.setText("Process Network");
		about.setText("About");
		help.setText("Help");
		restart.setText("Restart");
		quit.setText("Quit");
		eventLabel.setText("Event name:");
		durationLabel.setText("Duration:");
		dependenciesLabel.setText("Dependencies");
		startingEvent.setText("Starting Event?");
		startingEvent.setSelected(false);
		criticalPaths.setText("Show Only Critical Paths");
		criticalPaths.setSelected(false);
		eventName.setEditable(true);
		eventDuration.setEditable(true);
		eventDependencies.setEditable(true);
		
		//event listeners for the UI
		startingEvent.addItemListener(new StartingEventListener());
		addEvent.addActionListener(new AddEventListener());
		process.addActionListener(new AnalyzeListener());
		about.addActionListener(new AboutListener());
		help.addActionListener(new HelpListener());
		restart.addActionListener(new RestartListener());
		quit.addActionListener(new QuitListener());
		
		
	};
	
	public class AboutListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent action) {
			JOptionPane.showMessageDialog(null,"Programmed constructed by Team 2: Jacob Bos, Bailey Simon, Andrew Williams","About",JOptionPane.PLAIN_MESSAGE);
		}
	}
	public class HelpListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent action) {
			JOptionPane.showMessageDialog(null,"Head to https://goo.gl/kBtb6R for any assistance needed","Help",JOptionPane.PLAIN_MESSAGE);
		}
	}
	public class RestartListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent action) {
			int selectedOption=JOptionPane.showConfirmDialog(null,"Confirm Restart - All data will be erased","Restart", JOptionPane.YES_NO_OPTION);
			if (selectedOption==JOptionPane.YES_OPTION) {
			EventList.getInstance().resetList();
			}
		}
	}
	public class QuitListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent action) {
			int selectedOption=JOptionPane.showConfirmDialog(null,"Confirm Quit - All data will be lost","Quit",JOptionPane.YES_NO_OPTION);
			if (selectedOption==JOptionPane.YES_OPTION){
				EventList.getInstance().resetList();
				NetworkAnalysis.endApplication();
			}
		}
	}

	//Disables dependencies box is starting event box is checked
	public class StartingEventListener implements ItemListener{
		@Override
		public void itemStateChanged(ItemEvent event) {
			if (event.getStateChange()==ItemEvent.SELECTED) {
				eventDependencies.setEditable(false);
			} else {
				eventDependencies.setEditable(true);
			}
		}
	}
	
	public class AddEventListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent action) {
			EventList list = EventList.getInstance();
			boolean invalidEvent = false;
			int errorCode = -1;
			
			Event addEvent = null;
			if (eventDependencies.isEditable()) {
				//Tests to make sure all necessary field have been filled
				 
				if(eventDependencies.getText().length()==0){
					invalidEvent = true;
					errorCode = 0;
					//no dependencies
				}
				else if(eventDuration.getText().length()==0 || eventName.getText().length()==0){
					invalidEvent = true;
					errorCode = 2;
					//no duration / no name
				}
				else{
					//Enters dependencies into the array list
					ArrayList<String> events = new ArrayList<String>(Arrays.asList(eventDependencies.getText().split(",")));
					String name = eventName.getText();
					
					//check for node dependency on itself
					for(String node: events){
						if(name.equals(node)) {
							invalidEvent = true;
							errorCode = 3;
						}
					}
					
					//loop to store the duration values
					for (char number : eventDuration.getText().toCharArray()){
				        if (!Character.isDigit(number)){
				        	invalidEvent = true;
				        	errorCode = 1;
				        	break;
				        	//duration is not an integer
				        }
				    }
					
					//if duration is able to store, make new event
					if(!invalidEvent){
						int duration = Integer.parseInt(eventDuration.getText());
						addEvent = new Event(name, duration, events);
					}
				}
			} 
			else{
				if(eventDuration.getText().length()==0 || eventName.getText().length()==0){
					invalidEvent = true;
					errorCode = 0;
				}
				else{
					String name = eventName.getText();
					//loop to verify duration is storing a number
					for (char number : eventDuration.getText().toCharArray()){
				        if (!Character.isDigit(number)){
				        	invalidEvent = true;
				        	errorCode = 1;
				        	break;
				        }
				    }
					//if duration is store, new event is made
					if(!invalidEvent){
						int duration = Integer.parseInt(eventDuration.getText());
						addEvent = new Event(name, duration);
					}
				}
			}
			if(!invalidEvent && addEvent != null) {
				//event added
				list.addToList(addEvent);
				JOptionPane.showMessageDialog(null,"Added event " + addEvent.getName() + " with duration " + addEvent.getDuration(), "Event added",JOptionPane.PLAIN_MESSAGE);
			}
			else if(invalidEvent){
				if(errorCode == 0){
					JOptionPane.showMessageDialog(null,"Unable to add Event - Event was not a starting event and had no dependencies","Unable to add Event",JOptionPane.ERROR_MESSAGE);			
				}
				else if(errorCode == 1){
					JOptionPane.showMessageDialog(null,"Duration must be an integer","Unable to add Event",JOptionPane.ERROR_MESSAGE);			
				}
				else if(errorCode == 2){
					JOptionPane.showMessageDialog(null,"Event must contain a name and a duration","Unable to add Event",JOptionPane.ERROR_MESSAGE);			
				}
				else if(errorCode == 3){
					JOptionPane.showMessageDialog(null,"Event cannot depend on itself","Unable to add Event",JOptionPane.ERROR_MESSAGE);			
				}
			}
			eventDuration.setText("");
			eventName.setText("");
			if(eventDependencies.isEditable()){
				eventDependencies.setText("");
			}
		}
	}
	
	public class AnalyzeListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent action) 
		{
			EventList list = EventList.getInstance();
			Organizer organizingList = new Organizer(list);
			/*
			 * Tests to see if there are any errors in the nodeList
			 */
			organizingList.checkAll();
			/*
			 * If error found, goes through error codes to show correct message
			 */
			if(organizingList.isValid()==false)
			{
				if(Organizer.getErrorCode() == 0)
				{
					/**
					 * Display Message saying that there were multiple instances of a node
					 */
					{
						JOptionPane.showMessageDialog(null,"Could not determine paths because multiple nodes had the same name","Could Not Analyze",JOptionPane.ERROR_MESSAGE);
						EventList.getInstance().resetList();
					}
				}
				else if(Organizer.getErrorCode() == 1)
				{
					/**
					 * Display Message saying that there were multiple instances of a node
					 */
					{
						JOptionPane.showMessageDialog(null,"Could not determine paths because dependencies do not exist","Could Not Analyze",JOptionPane.ERROR_MESSAGE);
						EventList.getInstance().resetList();
					}
				}
				else if(Organizer.getErrorCode() == 2)
				{
					/**
					 * Display Message saying that there were multiple instances of a node
					 */
					{
						JOptionPane.showMessageDialog(null,"A circular path of nodes exist","Could Not Analyze",JOptionPane.ERROR_MESSAGE);		
						EventList.getInstance().resetList();
					}
				}
				else if(Organizer.getErrorCode() == 3)
				{
					/**
					 * 
					 */
					{
						JOptionPane.showMessageDialog(null,"All nodes are not properly connected to dependencies","Could Not Analyze",JOptionPane.ERROR_MESSAGE);	
						EventList.getInstance().resetList();
					}
				}
				else if(Organizer.getErrorCode() == 4)
				{
					/**
					 * 
					 */
					{
						JOptionPane.showMessageDialog(null,"No nodes have been added","Could Not Analyze",JOptionPane.ERROR_MESSAGE);	
						EventList.getInstance().resetList();
					}
				}
				/*if(Organizer.getErrorCode() == 6)
				{
					/**
					 * Display Message saying that there were multiple instances of a node
					 *
					{
						JOptionPane.showMessageDialog(null,"Node heads not connected","Could Not Analyze",JOptionPane.ERROR_MESSAGE);
						NodeList.getInstance().resetList();
					}
				}*/
			}
			
			/*
			 * If no errors found, proceeds to Analyze function
			 */
			else
			{
				/*
				 * Performs the recursive function to find all of the paths
				 */
				organizingList.recursiveStartPath();
				ArrayList<ArrayList<String>> pathStrings;
				ArrayList<Integer> pathDurations;
				if(!criticalPaths.isSelected())
				{
					copOut = false;
					/*
					 * Sets the pathList's value and organizes the pathList by value
					 */
					organizingList.preparePathList();
					/*
					 * List of Strings that are the name of nodes in each path
					 */
					pathStrings = new ArrayList<ArrayList<String>>(organizingList.getNames());
					/*
					 * List of length of each path
					 */
					pathDurations = new ArrayList<Integer>(organizingList.getDurations());
				}
				else
				{
					copOut = true;
					
					ArrayList<Path> criticalPaths = organizingList.getCritical();
					
					pathStrings = new ArrayList<ArrayList<String>>(organizingList.getCriticalNames(criticalPaths));
					
					pathDurations = new ArrayList<Integer>(organizingList.getCriticalDuration(criticalPaths));
				}
				//UI for showing analysis
				analysisFrame=new JFrame();
				analysisFrame.setTitle("Analysis Results");
				analysisFrame.setBounds(getX(), getY(), getWidth(), getHeight());
				analysisFrame.add(new AnalysisUI(pathStrings,pathDurations, organizingList));
				analysisFrame.setVisible(true);
			}
			//NodeList.getInstance().resetList();
		}
	}
}

