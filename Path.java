import java.util.ArrayList;

//Similar to a struct in C - holds path information
public class Path{
	public ArrayList<Event> path;
	public int duration;
	
	public Path(ArrayList<Event> path){
		this.path = path;
		duration = 0;
	}
	
	public void setDuration(){
		duration = 0;
		for(Event x: path){
			duration+=x.getDuration();
		}
	}
	
	public ArrayList<Event> getPath(){
		return path;
	}
	public int getDuration() {
		return duration;
	}
}
